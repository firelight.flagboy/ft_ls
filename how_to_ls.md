# How to ls
a short explanation how ls work
***
ls list file in directory
***
## How option work
* ls -a : ls all file that begin with '.'
* ls -R : list recursively file in dir
* ls -l : list file with data (type of file, mode, ower, groups, weight, date, name)
* ls -r : list fill in reverse ascii order
* ls -t : sort file by modified time
## Bonus
* ls -G : list file with color
* ls -u : use time of last acces for (-t and -l)
* ls -f : output is not sorted
* ls -d : dir are listed as plain file
* ls -g : display the group name when -l is specified (the owner name is suppressed)
* ls -1 : display output by colonne
