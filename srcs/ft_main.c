/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_main.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/27 13:02:26 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/22 14:50:43 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		ft_sort_tab(char **av, int (*f)(char const *, char const *))
{
	char	*s;
	int		i;

	i = 0;
	while (av[i + 1])
	{
		if (f(av[i], av[i + 1]) >= 0)
		{
			s = av[i];
			av[i] = av[i + 1];
			av[i + 1] = s;
			i = 0;
		}
		else
			i++;
	}
	return (0);
}

void	ft_get_sorting_fc_r(t_opt *opt)
{
	opt->cmp_name = ft_cmp_name_r;
	if (opt->opt & IS_MT)
	{
		opt->cmp = ft_cmp_date_r;
	}
	else
		opt->cmp = ft_cmp_name_r;
}

void	ft_get_sorting_fc(t_opt *opt)
{
	if (opt->opt & IS_MF)
	{
		opt->cmp = 0;
		opt->cmp_name = 0;
		return ;
	}
	if (opt->opt & IS_MR)
		return (ft_get_sorting_fc_r(opt));
	else
	{
		opt->cmp_name = ft_cmp_name;
		if (opt->opt & IS_MT)
		{
			opt->cmp = ft_cmp_date;
		}
		else
			opt->cmp = ft_cmp_name;
	}
}

int		main(int ac, char **av)
{
	t_opt			*opt;
	struct winsize	win;

	av++;
	ac--;
	opt = get_t_option();
	av = ft_get_option(&ac, av);
	opt->termwidth = 80;
	if (ioctl(0, TIOCGWINSZ, &win) == 0 && win.ws_col > 0)
		opt->termwidth = win.ws_col;
	if (opt->opt & IS_MF)
		ft_interpret_opt_f(opt);
	else if (opt->opt & IS_G)
		ft_interpret_opt_g(opt);
	else
		ft_interpret_opt(opt);
	if (!(opt->opt & (IS_R | IS_MD)))
		ft_ls(av, ac);
	else if (opt->opt & IS_MD)
		ft_ls_d(av);
	else
		ft_ls_rec(av, ac);
	return (opt->error);
}
