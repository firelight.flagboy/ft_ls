/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_routine.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/15 09:33:08 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/17 16:26:02 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ft_routine_list(t_tfile *file, t_stat *buf, char *name, char *path)
{
	t_opt *opt;

	opt = get_t_option();
	ft_strncpy(file->name, name, MAX_NAME);
	ft_strncpy(file->path, path, MAX_PATH);
	ft_add_fullpath(file->fullpath, path, name, MAX_PATH);
	if (opt->opt & IS_MA || file->name[0] != '.')
	{
		if (lstat(file->fullpath, buf) < 0)
		{
			file->error = 1;
			ft_warnx1(file->fullpath, errno);
		}
		else
			ft_get_mode(file, buf);
	}
}

void	ft_routine_c_mode(t_tfile *f, t_stat *buf, t_opt *opt)
{
	size_t l;

	f->rdev = buf->st_rdev;
	f->major = (__int32_t)(((__int32_t)f->rdev >> 24) & 0xff);
	f->minor = (__int32_t)((f->rdev) & 0xffffff);
	if ((l = ft_len_nb(f->major, 10)) > opt->major)
		opt->major = l;
	if ((l = ft_len_nb(f->minor, 10)) > opt->minor)
		opt->minor = l;
	if ((l = opt->major + opt->minor + 2) > opt->size)
		opt->size = l;
}

void	ft_routine_mode(t_tfile *f, t_stat *buf, t_opt *opt)
{
	size_t	l;
	ssize_t	b;

	f->size = buf->st_size;
	if ((l = ft_len_nb(f->size, 10)) > opt->size)
		opt->size = l;
	f->size_block = buf->st_blocks;
	opt->total += f->size_block;
	if (f->type == 'c')
		ft_routine_c_mode(f, buf, opt);
	f->time[0] = buf->st_ctime;
	f->time[1] = buf->st_atime;
	f->time[2] = buf->st_mtime;
	f->nb_link = buf->st_nlink;
	if ((l = ft_len_nb(f->nb_link, 10)) > opt->link)
		opt->link = l;
	if (f->type == 'l')
	{
		if ((b = readlink(f->fullpath, f->name_link, MAX_NAME)) < 0)
			ft_error(f->fullpath, errno);
		else
			f->name_link[b] = 0;
	}
}
