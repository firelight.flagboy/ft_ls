/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 14:08:24 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/16 14:20:45 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char	*ft_strncpy(char *d, char const *s, size_t n)
{
	size_t i;

	if (!d || !s)
		return (NULL);
	i = 0;
	while (i < n && s[i])
	{
		d[i] = s[i];
		i++;
	}
	d[i] = 0;
	return (d);
}

char	*ft_strrchr(char const *s, int c)
{
	char	*save;

	if (!s)
		return (NULL);
	save = NULL;
	while (*s)
	{
		if (*s == c)
			save = (char*)s;
		s++;
	}
	return (save);
}

char	*ft_add_fullpath\
	(char *d, char const *path, char const *name, size_t lim)
{
	size_t i;
	size_t p;

	if (name == NULL || d == NULL || path == NULL)
		return (NULL);
	i = 0;
	while (path[i] && lim)
	{
		d[i] = path[i];
		i++;
		lim--;
	}
	p = 0;
	while (name[p] && lim)
	{
		d[p + i] = name[p];
		p++;
		lim--;
	}
	if (lim == 0)
		ft_error_str("error full path too long");
	d[i + p] = 0;
	return (d);
}

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

char	*ft_strlcat(char *d, char const *s, size_t lim)
{
	char	*res;

	res = d;
	while (*d)
	{
		lim--;
		d++;
	}
	while (*s && lim)
	{
		*d = *s;
		lim--;
		s++;
		d++;
	}
	if (*s != 0 && lim == 0)
		ft_error_str_file(res, "filename too long");
	*d = 0;
	return (d);
}
