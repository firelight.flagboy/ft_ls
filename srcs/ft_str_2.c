/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 14:20:01 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/17 14:27:01 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char		*ft_strlcpy_warn\
	(char *d, char const *s, char const *name, char mode)
{
	size_t	l;
	size_t	i;

	l = ft_strlen(s);
	if (l + 1 > MAX_NAME)
	{
		if (mode == 1)
			ft_error_str_file(name, "username too big");
		else if (mode == 2)
			ft_error_str_file(name, "groups name too big");
	}
	i = 0;
	while (i < l)
	{
		d[i] = s[i];
		i++;
	}
	d[i] = 0;
	return (d);
}
