/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 10:13:30 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/18 14:13:26 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		ft_error(char const *type, const int error)
{
	ft_dprintf(2, "./ft_ls: %s: %s\n", type, strerror(error));
	exit(EXIT_FAILURE);
}

int		ft_bad_option(char opt)
{
	ft_dprintf(2,\
	"./ft_ls: illegal option -- %c\n\
usage : ./ft_ls [-GRadfglrtu1@] [file ...]\n"\
	, opt);
	exit(EXIT_FAILURE);
}

int		ft_error_str(char const *s)
{
	ft_dprintf(2, "./ft_ls: %s\n", s);
	exit(EXIT_FAILURE);
}

int		ft_error_str_file(char const *filename, char const *error)
{
	ft_dprintf(2, "./ft_ls: %s: %s\n", filename, error);
	exit(EXIT_FAILURE);
}
