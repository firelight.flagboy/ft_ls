/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_col.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 09:15:12 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/22 14:56:50 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		ft_print_normal(t_tfile *file, t_opt *opt)
{
	int	colwidth;
	int	numcols;
	int	col;

	colwidth = opt->name + 1;
	numcols = opt->termwidth / colwidth;
	colwidth = opt->termwidth / numcols;
	col = 0;
	while (file)
	{
		if (file->error != 1 && (opt->opt & IS_MA || file->name[0] != '.'))
		{
			if (col >= numcols && (col = 0) != -1)
				ft_putchar('\n');
			ft_printf("%-*s", colwidth, file->name);
			col++;
		}
		file = file->next;
	}
	ft_putchar('\n');
	return (0);
}

int		ft_print_normal_g(t_tfile *file, t_opt *opt)
{
	int	colwidth;
	int	numcols;
	int	col;

	colwidth = opt->name + 1;
	numcols = opt->termwidth / colwidth;
	colwidth = opt->termwidth / numcols;
	col = 0;
	while (file)
	{
		if (file->error != 1 && (opt->opt & IS_MA || file->name[0] != '.'))
		{
			if (col >= numcols && (col = 0) != -1)
				ft_putchar('\n');
			ft_printf("%s%-*s\033[0m",\
			ft_get_color(file), colwidth, file->name);
			col++;
		}
		file = file->next;
	}
	ft_putchar('\n');
	return (0);
}
