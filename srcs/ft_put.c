/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 11:29:02 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/18 10:33:13 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ft_putcol(t_tfile *f, t_opt *opt)
{
	ft_printf("%-*s\n", opt->name, f->name);
}

void	ft_putlong(t_tfile *file, t_opt *opt)
{
	char	*stime;

	stime = ft_get_time(file->time + opt->i_time);
	if (file->type == 'l')
		ft_printf("l%s%c %*d %-*s  %-*s  %*lu %s %s -> %s\n",\
		file->mode, file->ext, opt->link, file->nb_link,\
		opt->owner, file->owner, opt->groups, file->groups,\
		opt->size, file->size, stime, file->name, file->name_link);
	else if (file->type == 'c')
		ft_printf("c%s%c  %*d %-*s  %-*s  %*u, %*u %s %s\n",\
		file->mode, file->ext, opt->link, file->nb_link, opt->owner,\
		file->owner, opt->groups, file->groups, opt->major, file->major,\
		opt->minor, file->minor, stime, file->name);
	else
		ft_printf("%c%s%c %*d %-*s  %-*s  %*lu %s %s\n",\
		file->type, file->mode, file->ext, opt->link, file->nb_link,\
		opt->owner, file->owner, opt->groups, file->groups,\
		opt->size, file->size, stime, file->name);
	if ((opt->opt & IS_AT) && file->xattr & HAVE_EXT)
		ft_list_xattr(file);
}

void	ft_putgroups(t_tfile *file, t_opt *opt)
{
	char	*stime;

	stime = ft_get_time(file->time + opt->i_time);
	if (file->type == 'l')
		ft_printf("l%s%c %*d %-*s  %*lu %s %s -> %s\n",\
		file->mode, file->ext, opt->link, file->nb_link,\
		opt->groups, file->groups, opt->size, file->size,\
		stime, file->name, file->name_link);
	else if (file->type == 'c')
		ft_printf("c%s%c %*d %-*s  %*u %*u %s %s\n",\
		file->mode, file->ext, opt->link, file->nb_link, opt->groups,\
		file->groups, file->major, file->minor, stime, file->name);
	else
		ft_printf("%c%s%c %*d %-*s  %*lu %s %s\n",\
		file->type, file->mode, file->ext, opt->link, file->nb_link,\
		opt->groups, file->groups, opt->size, file->size, stime, file->name);
	if ((opt->opt & IS_AT) && file->xattr & HAVE_EXT)
		ft_list_xattr(file);
}
