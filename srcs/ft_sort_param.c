/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_param.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/11 13:55:10 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/18 11:18:35 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ft_sort_param(char **av, int ac)
{
	char	*tmp;
	int		i;
	int		j;
	char	s;

	i = 0;
	while (i < ac - 1)
	{
		j = 0;
		s = 0;
		while (j < ac - i - 1)
		{
			if (ft_strcmp(av[j], av[j + 1]) > 0)
			{
				tmp = av[j];
				av[j] = av[j + 1];
				av[j + 1] = tmp;
				s = 1;
			}
			j++;
		}
		if (s == 0)
			break ;
		i++;
	}
}
