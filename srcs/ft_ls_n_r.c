/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_n_r.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 14:52:26 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/22 14:54:24 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ft_some_line(t_tfile *head, t_opt *opt, void (*f)(t_tfile**, t_opt*))
{
	if (opt->opt & IS_MT)
		ft_merge_sort(&head, opt->cmp_name);
	ft_merge_sort(&head, opt->cmp);
	f(&head, opt);
}
