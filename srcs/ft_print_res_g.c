/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_res_g.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/17 12:26:43 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/17 14:23:56 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int			ft_print_long_g(t_tfile *file, t_opt *opt)
{
	if (ft_will_print(file, opt))
		ft_printf("total %zu\n", opt->total);
	if (opt->opt & IS_MA)
	{
		while (file)
		{
			if (file->error != 1)
				ft_putlong_g(file, opt);
			file = file->next;
		}
	}
	else
	{
		while (file)
		{
			if (file->name[0] != '.' && file->error != 1)
				ft_putlong_g(file, opt);
			file = file->next;
		}
	}
	return (0);
}

int			ft_print_groups_g(t_tfile *file, t_opt *opt)
{
	if (ft_will_print(file, opt))
		ft_printf("total %zu\n", opt->total);
	if (opt->opt & IS_MA)
	{
		while (file)
		{
			if (file->error != 1)
				ft_putgroups_g(file, opt);
			file = file->next;
		}
	}
	else
	{
		while (file)
		{
			if (file->name[0] != '.' && file->error != 1)
				ft_putgroups_g(file, opt);
			file = file->next;
		}
	}
	return (0);
}

int			ft_print_col_g(t_tfile *file, t_opt *opt)
{
	if (opt->opt & IS_MA)
	{
		while (file)
		{
			if (file->error != 1)
				ft_putcol_g(file, opt);
			file = file->next;
		}
	}
	else
	{
		while (file)
		{
			if (file->name[0] != '.' && file->error != 1)
				ft_putcol_g(file, opt);
			file = file->next;
		}
	}
	return (0);
}
