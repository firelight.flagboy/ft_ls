/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_rec.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 13:44:38 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/22 14:55:47 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int	ft_isok_dir(t_tfile *file, t_opt *opt)
{
	char	*n;

	n = file->name;
	if (!(opt->opt & IS_MA))
		if (n[0] == '.')
			return (0);
	if (file->type != 'd' || file->error == 1)
		return (0);
	if (n[0] == '.' && (n[1] == 0 || (n[1] == '.' && n[2] == 0)))
		return (0);
	return (1);
}

static void	ft_recursive_call(t_tfile **begin, t_opt *opt)
{
	t_tfile		*head;
	char		*path;

	if (opt->opt & IS_MT)
		ft_merge_sort(begin, opt->cmp_name);
	ft_merge_sort(begin, opt->cmp);
	opt->print(*begin, opt);
	head = *begin;
	while (head)
	{
		if (ft_isok_dir(head, opt))
		{
			path = head->fullpath;
			ft_reset_opt(opt);
			ft_printf("\n%s:\n", path);
			ft_strlcat(path, "/", MAX_PATH);
			ft_list_simple_r(path, opt);
		}
		head = head->next;
	}
	ft_tfile_free(begin);
}

int			ft_list_simple_r(char *path, t_opt *opt)
{
	DIR			*dir;
	t_dirent	*dirent;
	t_stat		buf;
	t_tfile		*file;
	t_tfile		*head;

	if (!(dir = opendir(path)))
		return (ft_warnx1(path, errno));
	if ((dirent = readdir(dir)))
	{
		file = ft_tfile_nelem();
		ft_routine_list(file, &buf, dirent->d_name, path);
		head = file;
	}
	while (dirent && (dirent = readdir(dir)))
	{
		file->next = ft_tfile_nelem();
		file = file->next;
		ft_routine_list(file, &buf, dirent->d_name, path);
	}
	closedir(dir);
	ft_recursive_call(&head, opt);
	return (0);
}

void		ft_ls_av_r(t_tfile **begin, t_opt *opt)
{
	t_tfile		*file;
	int			hp;

	file = *begin;
	hp = 0;
	while (file)
	{
		if (file->error != 1)
		{
			if (file->type != 'd')
				opt->put(file, opt);
			else
				hp += 1;
		}
		file = file->next;
	}
	if (hp > 1)
		ft_putchar('\n');
	ft_list_dir_r(begin, opt, hp);
	ft_tfile_free(begin);
}

int			ft_ls_rec(char **av, int ac)
{
	t_opt	*opt;
	t_tfile	*f;
	t_tfile	*head;
	t_stat	b;

	opt = get_t_option();
	if (*av == NULL)
	{
		if (lstat("./", &b))
			ft_error("./", errno);
		if (!(b.st_mode & S_IRUSR))
			return (ft_warnx2("./", "acces denied"));
		return (ft_list_simple_r("./", opt));
	}
	ft_sort_param(av, ac);
	f = ft_ls_file(*av++, opt);
	head = f;
	while (*av)
	{
		f->next = ft_ls_file(*av++, opt);
		f = f->next;
	}
	ft_some_line(head, opt, ft_ls_av_r);
	return (opt->error);
}
