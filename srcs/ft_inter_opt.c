/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_inter_opt.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/17 14:27:34 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/22 14:50:11 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void	ft_interpret_opt_fg(t_opt *opt)
{
	opt->i_time = 2;
	if (opt->opt & IS_MT && opt->opt & IS_MU)
		opt->i_time = 1;
	opt->print = ft_print_normal_g;
	opt->put = ft_putcol_g;
	if (opt->opt & IS_1)
	{
		opt->print = ft_print_col_g;
		opt->put = ft_putcol_g;
	}
}

void		ft_interpret_opt_f(t_opt *opt)
{
	opt->opt |= IS_MA;
	opt->i_time = 2;
	opt->st = stat;
	if (opt->opt & IS_MT && opt->opt & IS_MU)
		opt->i_time = 1;
	opt->cmp = 0;
	opt->cmp_name = 0;
	if (opt->opt & IS_G)
		return (ft_interpret_opt_fg(opt));
	opt->print = ft_print_normal;
	opt->put = ft_putcol;
	if (opt->opt & IS_1)
	{
		opt->print = ft_print_col;
		opt->put = ft_putcol;
	}
}

void		ft_interpret_opt(t_opt *opt)
{
	opt->i_time = 2;
	opt->print = ft_print_normal;
	opt->put = ft_putcol;
	if (opt->opt & IS_MT && opt->opt & IS_MU)
		opt->i_time = 1;
	if (opt->opt & IS_1)
	{
		opt->print = ft_print_col;
		opt->put = ft_putcol;
	}
	if (opt->opt & (IS_MG | IS_ML))
	{
		opt->st = lstat;
		if (opt->opt & IS_MG && (opt->print = ft_print_groups) != 0)
			opt->put = ft_putgroups;
		else if ((opt->print = ft_print_long) != 0)
			opt->put = ft_putlong;
	}
	else
		opt->st = stat;
	ft_get_sorting_fc(opt);
}

void		ft_interpret_opt_g(t_opt *opt)
{
	opt->i_time = 2;
	opt->print = ft_print_normal_g;
	opt->put = ft_putcol_g;
	if (opt->opt & IS_MT && opt->opt & IS_MU)
		opt->i_time = 1;
	if (opt->opt & IS_1)
	{
		opt->print = ft_print_col_g;
		opt->put = ft_putcol_g;
	}
	if (opt->opt & (IS_MG | IS_ML))
	{
		opt->st = lstat;
		if (opt->opt & IS_MG && (opt->print = ft_print_groups_g) != 0)
			opt->put = ft_putgroups_g;
		else if ((opt->print = ft_print_long_g) != 0)
			opt->put = ft_putlong_g;
	}
	else
		opt->st = stat;
	ft_get_sorting_fc(opt);
}
