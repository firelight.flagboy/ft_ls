/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_xattr.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/17 11:07:50 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/18 10:30:45 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		ft_list_xattr(t_tfile *f)
{
	char		value[XATTR_SIZE];
	ssize_t		val_l;
	int			ns;

	ns = 0;
	while (ns < f->xattrlen)
	{
		val_l =\
getxattr(f->fullpath, f->xattrlist + ns, value, XATTR_SIZE, 0, XATTR_NOFOLLOW);
		if (val_l == -1)
			return (ft_warnx1(f->fullpath, errno));
		ft_printf("\t%s\t%ld\n", f->xattrlist + ns, val_l);
		ns += ft_strlen(f->xattrlist + ns) + 1;
	}
	return (0);
}
