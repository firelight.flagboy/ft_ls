/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_merge.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/15 13:42:04 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/18 14:08:51 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static t_tfile	*ft_sorted_merge\
	(t_tfile *a, t_tfile *b, int (*cmp)(t_tfile*, t_tfile*))
{
	t_tfile *result;

	result = NULL;
	if (a == NULL)
		return (b);
	else if (b == NULL)
		return (a);
	if (cmp(a, b) <= 0)
	{
		result = a;
		result->next = ft_sorted_merge(a->next, b, cmp);
	}
	else
	{
		result = b;
		result->next = ft_sorted_merge(a, b->next, cmp);
	}
	return (result);
}

static void		ft_front_back_split\
	(t_tfile *source, t_tfile **front_ref, t_tfile **back_ref)
{
	t_tfile *fast;
	t_tfile *slow;

	if (source == NULL || source->next == NULL)
	{
		*front_ref = source;
		*back_ref = NULL;
	}
	else
	{
		slow = source;
		fast = source->next;
		while (fast != NULL)
		{
			fast = fast->next;
			if (fast != NULL)
			{
				slow = slow->next;
				fast = fast->next;
			}
		}
		*front_ref = source;
		*back_ref = slow->next;
		slow->next = NULL;
	}
}

void			ft_merge_sort\
	(t_tfile **head_ref, int (*cmp)(t_tfile*, t_tfile*))
{
	t_tfile *head;
	t_tfile *a;
	t_tfile *b;

	head = *head_ref;
	if ((head == NULL) || (head->next == NULL) || cmp == NULL)
		return ;
	ft_front_back_split(head, &a, &b);
	ft_merge_sort(&a, cmp);
	ft_merge_sort(&b, cmp);
	*head_ref = ft_sorted_merge(a, b, cmp);
}
