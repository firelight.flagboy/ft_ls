/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_file.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 11:10:53 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/18 11:11:03 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_tfile		*ft_ls_file(char *fullpath, t_opt *opt)
{
	t_tfile	*file;
	t_stat	buf;
	char	*s;

	file = ft_tfile_nelem();
	s = NULL;
	if (fullpath[0] != '/')
		s = ft_strrchr(fullpath, '/');
	ft_strncpy(file->fullpath, fullpath, MAX_PATH);
	if (opt->st(fullpath, &buf))
	{
		if (!s)
			ft_warnx1(fullpath, errno);
		else
			ft_warnx1(s + 1, errno);
		file->error = 1;
	}
	else
		ft_get_mode(file, &buf);
	ft_copy_data(&s, &fullpath, file);
	return (file);
}
