/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_struct.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 10:27:32 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/17 11:55:01 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	ft_init_toption_val(void *data)
{
	t_topt *t;

	t = data;
	t[0].val = IS_MA;
	t[1].val = IS_R;
	t[2].val = IS_ML;
	t[3].val = IS_MR;
	t[4].val = IS_MT;
	t[5].val = IS_G;
	t[6].val = IS_MU;
	t[7].val = IS_MF;
	t[8].val = IS_MD;
	t[9].val = IS_MG;
	t[10].val = IS_1;
	t[11].val = IS_AT;
	t[12].val = 0;
}

void	ft_init_toption_let(void *data)
{
	t_topt *t;

	t = data;
	t[0].l = 'a';
	t[1].l = 'R';
	t[2].l = 'l';
	t[3].l = 'r';
	t[4].l = 't';
	t[5].l = 'G';
	t[6].l = 'u';
	t[7].l = 'f';
	t[8].l = 'd';
	t[9].l = 'g';
	t[10].l = '1';
	t[11].l = '@';
	t[12].l = 0;
}
