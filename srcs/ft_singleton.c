/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_singleton.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 10:19:08 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/17 11:54:34 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_topt	*get_t_toption(void)
{
	static t_topt tab[NB_OPT + 1];

	if (tab[0].val == 0)
	{
		ft_init_toption_val((void*)tab);
		ft_init_toption_let((void*)tab);
	}
	return (tab);
}

t_opt	*get_t_option(void)
{
	static t_opt option;

	return (&option);
}

void	ft_reset_opt(t_opt *opt)
{
	opt->groups = 0;
	opt->name = 0;
	opt->size = 0;
	opt->link = 0;
	opt->major = 0;
	opt->minor = 0;
	opt->owner = 0;
	opt->total = 0;
}
