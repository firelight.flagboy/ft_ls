/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_mode.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 14:02:51 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/18 14:21:07 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int	ft_get_groups(t_tfile *f, t_stat *buf)
{
	t_passwd	*passwd;
	t_group		*gr;

	if (!(passwd = getpwuid(buf->st_uid)))
	{
		if (errno & (EINTR | EIO | EMFILE | ENFILE | ENOMEM | ERANGE)\
		&& (f->error = 1))
			return (ft_warnx1(f->fullpath, errno));
		else
			ft_strlcpy_warn(f->owner, ft_itoa_st(buf->st_uid), f->fullpath, 1);
	}
	else
		ft_strlcpy_warn(f->owner, passwd->pw_name, f->fullpath, 1);
	if (!(gr = getgrgid(buf->st_gid)))
	{
		if (errno & (EINTR | EIO | EMFILE | ENFILE | ENOMEM | ERANGE)\
		&& (f->error = 1))
			return (ft_warnx1(f->fullpath, errno));
		else
			ft_strlcpy_warn(f->groups, ft_itoa_st(buf->st_gid), f->fullpath, 2);
	}
	else
		ft_strlcpy_warn(f->groups, gr->gr_name, f->fullpath, 2);
	return (0);
}

static char	ft_get_type(t_stat *buf)
{
	if (S_ISREG(buf->st_mode))
		return ('-');
	if (S_ISDIR(buf->st_mode))
		return ('d');
	if (S_ISBLK(buf->st_mode))
		return ('b');
	if (S_ISFIFO(buf->st_mode))
		return ('p');
	if (S_ISLNK(buf->st_mode))
		return ('l');
	if (S_ISSOCK(buf->st_mode))
		return ('s');
	if (S_ISCHR(buf->st_mode))
		return ('c');
	return (0);
}

static void	ft_get_othmode(t_tfile *file, t_stat *buf)
{
	file->mode[9] = 0;
	file->mode[0] = (buf->st_mode & S_IRUSR) ? 'r' : '-';
	file->mode[1] = (buf->st_mode & S_IWUSR) ? 'w' : '-';
	if (buf->st_mode & S_ISUID)
		file->mode[2] = (buf->st_mode & S_IXUSR) ? 's' : 'S';
	else
		file->mode[2] = (buf->st_mode & S_IXUSR) ? 'x' : '-';
	file->mode[3] = (buf->st_mode & S_IRGRP) ? 'r' : '-';
	file->mode[4] = (buf->st_mode & S_IWGRP) ? 'w' : '-';
	if (buf->st_mode & S_ISGID)
		file->mode[5] = (buf->st_mode & S_IXGRP) ? 's' : 'S';
	else
		file->mode[5] = (buf->st_mode & S_IXGRP) ? 'x' : '-';
	file->mode[6] = (buf->st_mode & S_IROTH) ? 'r' : '-';
	file->mode[7] = (buf->st_mode & S_IWOTH) ? 'w' : '-';
	if (buf->st_mode & S_ISVTX)
		file->mode[8] = (buf->st_mode & S_IXOTH) ? 't' : 'T';
	else
		file->mode[8] = (buf->st_mode & S_IXOTH) ? 'x' : '-';
}

static int	ft_get_info(t_tfile *file, t_stat *buf, t_opt *opt)
{
	size_t		l;

	if (ft_get_groups(file, buf) == 1)
		return (1);
	if ((l = ft_strlen(file->owner)) > opt->owner)
		opt->owner = l;
	if ((l = ft_strlen(file->groups)) > opt->groups)
		opt->groups = l;
	if ((l = ft_strlen(file->name)) > opt->name)
		opt->name = l;
	ft_routine_mode(file, buf, opt);
	return (0);
}

int			ft_get_mode(t_tfile *file, t_stat *buf)
{
	t_opt *opt;

	opt = get_t_option();
	if ((file->type = ft_get_type(buf)) == 0)
		return (ft_error_str_file(file->fullpath, "what a new file type ???"));
	ft_get_othmode(file, buf);
	if (opt->opt & (IS_MG | IS_ML))
	{
		if (file->type != 'c' && file->type != 'b')
			ft_get_xattr(file, opt);
		else
			file->xattr |= 0;
		ft_get_acl(file);
		if (file->xattr & HAVE_EXT)
			file->ext = '@';
		else if (file->xattr & HAVE_ACL)
			file->ext = '+';
		else
			file->ext = ' ';
	}
	return (ft_get_info(file, buf, opt));
}
