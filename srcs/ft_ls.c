/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 13:44:01 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/22 14:55:34 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void	ft_list_simple_rot(t_tfile **head, t_opt *opt)
{
	if (opt->opt & IS_MT)
		ft_merge_sort(head, opt->cmp_name);
	ft_merge_sort(head, opt->cmp);
	opt->print(*head, opt);
	ft_tfile_free(head);
}

int			ft_list_simple(char *path, t_opt *opt)
{
	DIR			*dir;
	t_dirent	*dirent;
	t_stat		buf;
	t_tfile		*file;
	t_tfile		*head;

	if (!(dir = opendir(path)))
		return (ft_warnx1(path, errno));
	if ((dirent = readdir(dir)))
	{
		file = ft_tfile_nelem();
		ft_routine_list(file, &buf, dirent->d_name, path);
		head = file;
	}
	while (dirent && (dirent = readdir(dir)))
	{
		file->next = ft_tfile_nelem();
		file = file->next;
		ft_routine_list(file, &buf, dirent->d_name, path);
	}
	closedir(dir);
	ft_list_simple_rot(&head, opt);
	return (0);
}

void		ft_list_dir(t_tfile **head, t_opt *opt, int count)
{
	t_tfile	*d;
	char	p;

	d = *head;
	if (count > 1)
		p = 1;
	else
		p = 0;
	while (d)
	{
		if (d->type == 'd' && d->error != 1)
		{
			ft_reset_opt(opt);
			if (p || opt->error)
				ft_printf("%s:\n", d->path);
			ft_list_simple(d->path, opt);
			count--;
			if (count)
				ft_putchar('\n');
		}
		d = d->next;
	}
}

void		ft_ls_av(t_tfile **list, t_opt *opt)
{
	t_tfile		*file;
	int			hp;

	file = *list;
	hp = 0;
	while (file)
	{
		if (file->error != 1)
		{
			if (file->type != 'd')
				opt->put(file, opt);
			else
				hp += 1;
		}
		file = file->next;
	}
	if (hp > 1)
		ft_putchar('\n');
	ft_list_dir(list, opt, hp);
	ft_tfile_free(list);
}

int			ft_ls(char **av, int ac)
{
	t_opt	*opt;
	t_tfile	*f;
	t_tfile *head;
	t_stat	b;

	opt = get_t_option();
	if (*av == NULL)
	{
		if (lstat("./", &b))
			ft_error("./", errno);
		if (!(b.st_mode & S_IRUSR))
			ft_warnx2("./", "acces denied");
		return (ft_list_simple("./", opt));
	}
	ft_sort_param(av, ac);
	f = ft_ls_file(*av++, opt);
	head = f;
	while (*av)
	{
		f->next = ft_ls_file(*av++, opt);
		f = f->next;
	}
	ft_some_line(head, opt, ft_ls_av);
	return (opt->error);
}
