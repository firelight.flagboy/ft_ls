/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_time_file.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 11:48:54 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/16 11:31:41 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		ft_abs(int nb)
{
	if (nb >= 0)
		return (nb);
	return (-nb);
}

char	*ft_get_time(time_t *ttime)
{
	static char	res[13];
	time_t		actual;
	int			y[2];
	char		*s;

	time(&actual);
	s = ctime(ttime);
	s += 4;
	ft_strncpy(res, s, 7);
	y[0] = (actual / SEC_PER_YEAR) + 1970;
	y[1] = (*ttime / SEC_PER_YEAR) + 1970;
	if (ft_abs(y[0] - y[1]) >= 2)
		ft_strncpy(res + 7, s + 15, 5);
	else
		ft_strncpy(res + 7, s + 7, 5);
	return (res);
}
