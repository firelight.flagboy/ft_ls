/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_res.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 15:54:22 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/18 11:28:59 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int			ft_will_print(t_tfile *file, t_opt *opt)
{
	if (opt->opt & IS_MD)
		return (0);
	if (opt->opt & IS_MA)
	{
		while (file)
		{
			if (file->error != 1)
				return (1);
			file = file->next;
		}
	}
	else
	{
		while (file)
		{
			if (file->error != 1 && file->name[0] != '.')
				return (1);
			file = file->next;
		}
	}
	return (0);
}

int			ft_print_long(t_tfile *file, t_opt *opt)
{
	if (ft_will_print(file, opt))
		ft_printf("total %zu\n", opt->total);
	if (opt->opt & IS_MA)
	{
		while (file)
		{
			if (file->error != 1)
				ft_putlong(file, opt);
			file = file->next;
		}
	}
	else
	{
		while (file)
		{
			if (file->name[0] != '.' && file->error != 1)
				ft_putlong(file, opt);
			file = file->next;
		}
	}
	return (0);
}

int			ft_print_groups(t_tfile *file, t_opt *opt)
{
	if (ft_will_print(file, opt))
		ft_printf("total %zu\n", opt->total);
	if (opt->opt & IS_MA)
	{
		while (file)
		{
			if (file->error != 1)
				ft_putgroups(file, opt);
			file = file->next;
		}
	}
	else
	{
		while (file)
		{
			if (file->name[0] != '.' && file->error != 1)
				ft_putgroups(file, opt);
			file = file->next;
		}
	}
	return (0);
}

int			ft_print_col(t_tfile *file, t_opt *opt)
{
	if (opt->opt & IS_MA)
	{
		while (file)
		{
			if (file->error != 1)
				ft_putcol(file, opt);
			file = file->next;
		}
	}
	else
	{
		while (file)
		{
			if (file->name[0] != '.' && file->error != 1)
				ft_putcol(file, opt);
			file = file->next;
		}
	}
	return (0);
}
