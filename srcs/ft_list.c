/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 12:11:24 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/16 11:31:58 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_tfile		*ft_tfile_nelem(void)
{
	t_tfile	*file;

	if (!(file = (t_tfile*)malloc(sizeof(t_tfile))))
		ft_error("malloc t_tfile", errno);
	file->next = NULL;
	return (file);
}

void		ft_tfile_free(t_tfile **begin)
{
	t_tfile	*tmp;
	t_tfile	*node;

	if (begin == NULL || *begin == NULL)
		return ;
	node = *begin;
	while (node)
	{
		tmp = node;
		node = node->next;
		free(tmp);
	}
	*begin = NULL;
}
