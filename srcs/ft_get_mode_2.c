/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_mode_2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/17 10:24:57 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/18 10:23:13 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		ft_get_xattr(t_tfile *file, t_opt *opt)
{
	ssize_t len;

	if (file->mode[0] == '-')
	{
		file->xattr = ' ';
		return (0);
	}
	if ((len =\
		listxattr(file->fullpath, file->xattrlist, XATTR_SIZE, XATTR_NOFOLLOW))\
		== -1)
	{
		file->xattr = 0;
		return ((opt->error = 1));
	}
	else if (len == 0)
		file->xattr = 0;
	else
		file->xattr = HAVE_EXT;
	file->xattrlen = len;
	return (0);
}

int		ft_get_acl(t_tfile *file)
{
	acl_t acl;

	if (!(acl = acl_get_file(file->fullpath, ACL_TYPE_EXTENDED)))
		return (0);
	file->xattr |= HAVE_ACL;
	acl_free((void*)acl);
	return (0);
}
