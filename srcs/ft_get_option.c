/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_option.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 10:32:41 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/18 14:13:24 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void	ft_rd_opt(char *s, t_topt *topt, t_opt *opt)
{
	int i;

	while (*s)
	{
		i = -1;
		while (topt[++i].l)
			if (topt[i].l == *s)
			{
				opt->opt |= topt[i].val;
				break ;
			}
		if (topt[i].l == 0)
			ft_bad_option(*s);
		s++;
	}
}

char		**ft_get_option(int *ac, char **av)
{
	t_topt	*topt;
	t_opt	*opt;
	int		i;

	opt = get_t_option();
	topt = get_t_toption();
	i = 0;
	while (i < *ac)
	{
		if (av[i][0] != '-')
			break ;
		if (av[i][1] == '-')
		{
			i++;
			break ;
		}
		ft_rd_opt(av[i] + 1, topt, opt);
		i++;
	}
	*ac = *ac - i;
	return (av + i);
}
