/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_rec_2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/17 14:31:43 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/18 11:41:59 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void		ft_list_dir_r(t_tfile **head, t_opt *opt, int count)
{
	t_tfile		*d;
	char		p;

	d = *head;
	if (count > 1)
		p = 1;
	else
		p = 0;
	while (d)
	{
		if (d->type == 'd' && d->error != 1)
		{
			ft_reset_opt(opt);
			if (p || opt->error)
				ft_printf("%s:\n", d->name);
			ft_list_simple_r(d->path, opt);
			count--;
			if (count)
				ft_putchar('\n');
		}
		d = d->next;
	}
}
