/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_warnx.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 11:27:55 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/16 14:55:45 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		ft_warnx1(char const *type, const int error)
{
	get_t_option()->error = 1;
	ft_dprintf(2, "./ft_ls: %s: %s\n", type, strerror(error));
	return (1);
}

int		ft_warnx2(char const *type, char const *error)
{
	get_t_option()->error = 1;
	ft_dprintf(2, "./ft_ls: %s: %s\n", type, error);
	return (1);
}
