/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_d.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/12 14:43:49 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/18 11:26:27 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void	ft_rt_av_null(t_opt *opt)
{
	t_tfile	*f;
	t_stat	b;
	t_tfile	*head;

	f = ft_tfile_nelem();
	head = f;
	if (lstat("./", &b))
		ft_error("./", errno);
	if (!(b.st_mode & S_IRUSR))
		ft_error_str_file("./", "acess denied");
	ft_get_mode(f, &b);
	ft_strncpy(f->name, ".", MAX_NAME);
	opt->put(f, opt);
	ft_tfile_free(&head);
}

t_tfile		*ft_ls_file_d(char *fullpath, t_opt *opt)
{
	t_tfile *file;
	t_stat	buf;
	char	*s;

	file = ft_tfile_nelem();
	s = NULL;
	if (fullpath[0] != '/')
		s = ft_strrchr(fullpath, '/');
	ft_strncpy(file->fullpath, fullpath, MAX_PATH);
	if (opt->st(fullpath, &buf))
	{
		if (!s)
			ft_warnx1(fullpath, errno);
		else
			ft_warnx1(s + 1, errno);
		file->error = 1;
	}
	else
		ft_get_mode(file, &buf);
	ft_copy_data(&s, &fullpath, file);
	return (file);
}

int			ft_ls_d(char **av)
{
	t_opt	*opt;
	t_tfile	*f;
	t_tfile	*head;

	opt = get_t_option();
	if (*av == NULL)
	{
		ft_rt_av_null(opt);
		return (0);
	}
	f = ft_ls_file_d(*av++, opt);
	head = f;
	while (*av)
	{
		f->next = ft_ls_file_d(*av++, opt);
		f = f->next;
	}
	if (opt->opt & IS_MT)
		ft_merge_sort(&head, opt->cmp_name);
	ft_merge_sort(&head, opt->cmp);
	opt->print(head, opt);
	ft_tfile_free(&head);
	return (0);
}
