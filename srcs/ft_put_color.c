/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_put_color.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/17 11:55:48 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/22 14:56:31 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char		*ft_get_color(t_tfile *file)
{
	char type;

	type = file->type;
	if (type == '-')
	{
		if (file->mode[2] == 'x')
			return ("\033[31m");
		else
			return ("\033[39m");
	}
	if (type == 'd')
		return ("\033[1;36m");
	if (type == 'l')
		return ("\033[95m");
	if (type == 'c')
		return ("\033[43;34m");
	if (type == 'p')
		return ("\033[33m");
	if (type == 's')
		return ("\033[106;91m");
	if (type == 'b')
		return ("\033[106;94m");
	return ("\033[0m");
}

void		ft_putcol_g(t_tfile *f, t_opt *opt)
{
	(void)opt;
	ft_printf("%s%s\033[0m\n", ft_get_color(f), f->name);
}

void		ft_putlong_g(t_tfile *file, t_opt *opt)
{
	char	*stime;

	if (file->error == 1)
		return ;
	stime = ft_get_time(file->time + opt->i_time);
	if (file->type == 'l')
		ft_printf("l%s%c %*d %-*s  %-*s  %*lu %s \033[35m%s\033[0m -> %s\n",\
		file->mode, file->ext, opt->link, file->nb_link,\
		opt->owner, file->owner, opt->groups, file->groups,\
		opt->size, file->size, stime, file->name, file->name_link);
	else if (file->type == 'c')
		ft_printf("c%s%c  %*d %-*s  %-*s  %*u, %*u %s \033[34;43m%s\033[0m\n",\
		file->mode, file->ext, opt->link, file->nb_link, opt->owner,\
		file->owner, opt->groups, file->groups, opt->major, file->major,\
		opt->minor, file->minor, stime, file->name);
	else
		ft_printf("%c%s%c %*d %-*s  %-*s  %*lu %s %s%s\033[0m\n",\
		file->type, file->mode, file->ext, opt->link, file->nb_link,\
		opt->owner, file->owner, opt->groups, file->groups,\
		opt->size, file->size, stime, ft_get_color(file), file->name);
	if ((opt->opt & IS_AT) && file->xattr | HAVE_EXT)
		ft_list_xattr(file);
}

void		ft_putgroups_g(t_tfile *file, t_opt *opt)
{
	char	*stime;

	if (file->error == 1)
		return ;
	stime = ft_get_time(file->time + opt->i_time);
	if (file->type == 'l')
		ft_printf("l%s%c %*d %-*s  %*lu %s \033[35m%s\033[0m -> %s\n",\
		file->mode, file->ext, opt->link, file->nb_link,\
		opt->groups, file->groups, opt->size, file->size,\
		stime, file->name, file->name_link);
	else if (file->type == 'c')
		ft_printf("c%s%c %*d %-*s  %*u %*u %s \033[43;34m%s\033[49;0m\n",\
		file->mode, file->ext, opt->link, file->nb_link, opt->groups,\
		file->groups, file->major, file->minor, stime, file->name);
	else
		ft_printf("%c%s%c %*d %-*s  %*lu %s %s%s\033[0m\n",\
		file->type, file->mode, file->ext, opt->link, file->nb_link,\
		opt->groups, file->groups, opt->size, file->size, stime,\
		ft_get_color(file), file->name);
	if ((opt->opt & IS_AT) && file->xattr | HAVE_EXT)
		ft_list_xattr(file);
}
