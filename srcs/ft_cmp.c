/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cmp.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/15 14:01:26 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/18 11:18:39 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		ft_cmp_name(t_tfile *a, t_tfile *b)
{
	return (ft_strcmp(a->name, b->name));
}

int		ft_cmp_date(t_tfile *a, t_tfile *b)
{
	int	i;

	i = get_t_option()->i_time;
	return (b->time[i] - a->time[i]);
}

int		ft_cmp_name_r(t_tfile *a, t_tfile *b)
{
	return (ft_strcmp(b->name, a->name));
}

int		ft_cmp_date_r(t_tfile *a, t_tfile *b)
{
	int	i;

	i = get_t_option()->i_time;
	return (a->time[i] - b->time[i]);
}
