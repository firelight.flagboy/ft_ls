/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_copy_data.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 11:32:49 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/16 11:33:38 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int	ft_add_slash(char *s)
{
	int i;

	i = 0;
	while (s[i])
		i++;
	if (i == 0 || s[i - 1] != '/')
		return (1);
	return (0);
}

void		ft_copy_data(char **s, char **fullpath, t_tfile *file)
{
	char	*str;
	char	*fp;

	str = *s;
	fp = *fullpath;
	if (str)
	{
		ft_strncpy(file->name, str + 1, MAX_NAME);
		ft_strncpy(file->path, fp, str - fp + 1);
	}
	else if (file->type == 'd')
	{
		ft_strncpy(file->name, fp, MAX_NAME);
		ft_strncpy(file->path, fp, MAX_PATH);
		if (ft_add_slash(file->path))
			ft_strlcat(file->path, "/", MAX_PATH);
	}
	else
	{
		ft_strncpy(file->name, fp, MAX_NAME);
		ft_strncpy(file->path, "./", MAX_PATH);
	}
}
