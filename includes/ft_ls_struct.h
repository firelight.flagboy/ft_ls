/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_struct.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 09:53:31 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/22 09:32:18 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_STRUCT_H
# define FT_LS_STRUCT_H

# include "ft_ls_o_include.h"
# include "ft_ls_define.h"

struct	s_toption
{
	char			l;
	unsigned short	val;
};

struct	s_tfile
{
	char			path[MAX_PATH];
	char			fullpath[MAX_PATH];
	char			name[MAX_NAME];
	char			mode[MOD_SIZE];
	char			owner[MAX_NAME];
	char			groups[MAX_NAME];
	char			name_link[MAX_NAME];
	char			xattrlist[XATTR_SIZE];
	char			xattr;
	char			ext;
	char			type;
	char			attr;
	int				nb_link;
	ssize_t			xattrlen;
	off_t			size;
	blkcnt_t		size_block;
	dev_t			rdev;
	uint			major;
	uint			minor;
	time_t			time[3];
	char			error;
	struct s_tfile	*next;
};

struct	s_option
{
	unsigned short	opt;
	char			error;
	int				(*cmp)(struct s_tfile *, struct s_tfile *);
	int				(*cmp_name)(struct s_tfile *, struct s_tfile *);
	int				(*print)(struct s_tfile *, struct s_option *);
	void			(*put)(struct s_tfile *, struct s_option *);
	int				(*st)(const char *, struct stat *);
	int				termwidth;
	size_t			name;
	size_t			groups;
	size_t			owner;
	size_t			link;
	size_t			size;
	size_t			major;
	size_t			minor;
	size_t			total;
	char			i_time;
};

#endif
