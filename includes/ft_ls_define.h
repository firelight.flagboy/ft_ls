/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_define.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 09:57:52 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/18 14:26:12 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_DEFINE_H
# define FT_LS_DEFINE_H

# ifndef NB_OPT
#  define NB_OPT 10
# endif

# ifndef MAX_PATH
#  define MAX_PATH 2048
# endif

# ifndef HAVE_EXT
#  define HAVE_EXT 0b1
# endif

# ifndef HAVE_ACL
#  define HAVE_ACL 0b10
# endif

# ifndef MAX_NAME
#  define MAX_NAME 1024
# endif

# ifndef MOD_SIZE
#  define MOD_SIZE 12
# endif

# ifndef XATTR_SIZE
#  define XATTR_SIZE 2048
# endif

# ifndef SEC_PER_YEAR
#  define SEC_PER_YEAR 31556952
# endif

# ifndef SEC_PER_MONTH
#  define SEC_PER_MONTH 2629800
# endif
/*
** M stand for minus
*/
# ifndef IS_MA
#  define IS_MA 0b1
# endif

# ifndef IS_R
#  define IS_R 0b10
# endif

# ifndef IS_ML
#  define IS_ML 0b100
# endif

# ifndef IS_MR
#  define IS_MR 0b1000
# endif

# ifndef IS_MT
#  define IS_MT 0b10000
# endif

# ifndef IS_G
#  define IS_G 0b100000
# endif

# ifndef IS_MU
#  define IS_MU 0b1000000
# endif

# ifndef IS_MF
#  define IS_MF 0b10000000
# endif

# ifndef IS_MD
#  define IS_MD 0b100000000
# endif

# ifndef IS_MG
#  define IS_MG 0b1000000000
# endif

# ifndef IS_1
#  define IS_1 0b10000000000
# endif

# ifndef IS_AT
#  define IS_AT 0b100000000000
# endif
#endif
