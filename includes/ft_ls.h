/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/27 12:39:48 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/22 14:54:48 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include "ft_ls_o_include.h"
# include "ft_printf.h"
# include "ft_ls_define.h"
# include "ft_ls_typedef.h"

/*
** Ft_ls
*/
int			ft_ls(char **av, int ac);
int			ft_ls_rec(char **av, int ac);
int			ft_ls_d(char **av);
void		ft_list(char *path);
char		*ft_get_color(t_tfile *file);
int			ft_will_print(t_tfile *file, t_opt *opt);
void		ft_interpret_opt(t_opt *opt);
void		ft_interpret_opt_f(t_opt *opt);
void		ft_interpret_opt_g(t_opt *opt);
void		ft_list_dir_r(t_tfile **head, t_opt *opt, int count);
void		ft_get_sorting_fc(t_opt *opt);
int			ft_get_acl(t_tfile *file);
t_tfile		*ft_ls_file(char *fullpath, t_opt *opt);
int			ft_list_simple_r(char *path, t_opt *opt);
int			ft_list_xattr(t_tfile *file);
void		ft_some_line\
	(t_tfile *head, t_opt *opt, void (*f)(t_tfile**, t_opt*));
void		ft_copy_data(char **s, char **fullpath, t_tfile *file);
char		*ft_add_fullpath\
	(char *d, char const *path, char const *name, size_t lim);
/*
** Routine
*/
void		ft_routine_list(t_tfile *file, t_stat *buf, char *name, char *path);
void		ft_routine_mode(t_tfile *f, t_stat *buf, t_opt *opt);
void		ft_routine_c_mode(t_tfile *f, t_stat *buf, t_opt *opt);
/*
** Print
*/
void		ft_print_option(void);
void		ft_print_mode(t_tfile *file);
void		ft_print(t_tfile *node);
int			ft_print_res(t_tfile *file);
void		ft_putchar(char c);
int			ft_print_normal(t_tfile *file, t_opt *opt);
int			ft_print_normal_g(t_tfile *file, t_opt *opt);
int			ft_print_long(t_tfile *file, t_opt *opt);
int			ft_print_long_g(t_tfile *file, t_opt *opt);
int			ft_print_groups(t_tfile *file, t_opt *opt);
int			ft_print_groups_g(t_tfile *file, t_opt *opt);
int			ft_print_col(t_tfile *file, t_opt *opt);
int			ft_print_col_g(t_tfile *file, t_opt *opt);
/*
** Put
*/
void		ft_putlong(t_tfile *file, t_opt *opt);
void		ft_putlong_g(t_tfile *file, t_opt *opt);
void		ft_putcol(t_tfile *f, t_opt *opt);
void		ft_putcol_g(t_tfile *f, t_opt *opt);
void		ft_putgroups(t_tfile *file, t_opt *opt);
void		ft_putgroups_g(t_tfile *file, t_opt *opt);
/*
** Sorting
*/
void		ft_merge_sort(t_tfile **head_ref, int (*cmp)(t_tfile*, t_tfile*));
void		ft_sort_param(char **av, int ac);
/*
** CMP
*/
int			ft_cmp_name(t_tfile *a, t_tfile *b);
int			ft_cmp_date(t_tfile *a, t_tfile *b);
int			ft_cmp_name_r(t_tfile *a, t_tfile *b);
int			ft_cmp_date_r(t_tfile *a, t_tfile *b);
/*
** LIBC
*/
char		*ft_strncpy(char *d, char const *s, size_t n);
char		*ft_strrchr(char const *s, int c);
char		*ft_strlcat(char *d, char const *s, size_t lim);
char		*ft_itoa_st(int nb);
void		ft_putchar(char c);
char		*ft_strlcpy_warn\
	(char *d, char const *s, char const *name, char mode);
/*
** Getter
*/
int			ft_get_xattr(t_tfile *file, t_opt *opt);
char		**ft_get_option(int *ac, char **av);
int			ft_get_mode(t_tfile *file, t_stat *buf);
char		*ft_get_time(time_t *ttime);
/*
** Singleton
*/
t_opt		*get_t_option();
t_topt		*get_t_toption();
void		ft_reset_opt(t_opt *opt);
/*
** Init Struct
*/
void		ft_init_toption_let(void *data);
void		ft_init_toption_val(void *data);
/*
** List
*/
t_tfile		*ft_tfile_nelem(void);
void		ft_tfile_free(t_tfile	**begin);
/*
** Error
*/
int			ft_error_str_file(char const *filename, char const *error);
int			ft_error(char const *type, const int error);
int			ft_bad_option(char opt);
int			ft_error_str(char const *s);
/*
** Warnx
*/
int			ft_warnx1(char const *type, const int error);
int			ft_warnx2(char const *file, char const *error);
#endif
