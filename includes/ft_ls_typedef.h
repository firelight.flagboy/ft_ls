/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_typedef.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 09:52:39 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/16 11:24:05 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_TYPEDEF_H
# define FT_LS_TYPEDEF_H

# include "ft_ls_struct.h"

/*
** Official struct
*/
typedef struct stat			t_stat;
typedef struct passwd		t_passwd;
typedef struct group		t_group;
typedef struct dirent		t_dirent;
/*
** Custom struct
*/
typedef struct s_option		t_opt;
typedef struct s_toption	t_topt;
typedef struct s_tfile		t_tfile;
#endif
