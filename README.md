# ft_ls
rebuild the classic command ls in C
***
## Info
### Pdf of project
* **[Links to pdf](https://cdn.intra.42.fr/pdf/pdf/17/ft_ls.fr.pdf)**
### Man
* **[man ls](http://manpagesfr.free.fr/man/man1/ls.1.html)**
* **[man readdir](http://manpagesfr.free.fr/man/man3/readdir.3.html)**
* **[man opendir](http://manpagesfr.free.fr/man/man3/opendir.3.html)**
* **[man closedir](http://manpagesfr.free.fr/man/man3/closedir.3.html)**
* **[man stat](http://manpagesfr.free.fr/man/man2/stat.2.html)**
* **[man lstat](http://manpagesfr.free.fr/man/man2/stat.2.html)**
* **[man getpwuid](http://manpagesfr.free.fr/man/man3/getpwnam.3.html)**
* **[man getgrgid](http://manpagesfr.free.fr/man/man3/getgrnam.3.html)**
* **[man listxattr](http://manpagesfr.free.fr/man/man2/listxattr.2.html)**
* **[man getxattr](http://manpagesfr.free.fr/man/man2/getxattr.2.html)**
* **[man time](http://manpagesfr.free.fr/man/man2/time.2.html)**
* **[man ctime](http://manpagesfr.free.fr/man/man3/ctime.3.html)**
* **[man readlink](http://manpagesfr.free.fr/man/man2/readlink.2.html)**
* **[man perror](http://manpagesfr.free.fr/man/man3/perror.3.html)**
* **[man strerror](http://manpagesfr.free.fr/man/man3/strerror.3.html)**
***
### Use
```
make ft_ls
./ft_ls <option> <directory>
```
***
### Option
* -a : list all file and hidden file (begin with '.')
* -R : list file recursively
* -l : long format output
* -r : reverse sort
* -t : use time instead of name for sort
* -G : list file with color
* -u : use time of last acces for (-t and -l)
* -f : output is not sorted
* -d : dir are listed as plain file
* -g : display the group name when -l is specified (the owner name is suppressed)
* -1 : display output by colonne
***
