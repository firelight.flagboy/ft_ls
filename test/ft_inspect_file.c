/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_inspect_file.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/27 13:37:09 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/08 14:53:04 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

typedef struct stat t_stat;
typedef struct passwd t_passwd;
typedef struct group t_group;

int		ft_print_type(t_stat *buf)
{
	if (S_ISREG(buf->st_mode))
		return (printf("- file"));
	if (S_ISDIR(buf->st_mode))
		return (printf("d dir"));
	if (S_ISBLK(buf->st_mode))
		return (printf("b block"));
	if (S_ISFIFO(buf->st_mode))
		return (printf("p fifo|tube"));
	if (S_ISLNK(buf->st_mode))
		return (printf("l lien"));
	if (S_ISSOCK(buf->st_mode))
		return (printf("s sock"));
	if (S_ISCHR(buf->st_mode))
		return (printf("c char"));
	return (0);
}

int		ft_print_mode_i(t_stat *buf)
{
	char s[10];

	s[9] = 0;
	s[0] = (buf->st_mode & S_IRUSR) ? 'r' : '-';
	s[1] = (buf->st_mode & S_IWUSR) ? 'w' : '-';
	s[2] = (buf->st_mode & S_IXUSR) ? 'x' : '-';
	s[3] = (buf->st_mode & S_IRGRP) ? 'r' : '-';
	s[4] = (buf->st_mode & S_IWGRP) ? 'w' : '-';
	s[5] = (buf->st_mode & S_IXGRP) ? 'x' : '-';
	s[6] = (buf->st_mode & S_IROTH) ? 'r' : '-';
	s[7] = (buf->st_mode & S_IWOTH) ? 'w' : '-';
	s[8] = (buf->st_mode & S_IXOTH) ? 'x' : '-';

	printf("%s\n", s);
	return (0);
}

void	ft_print_info(t_stat *buf)
{
	t_passwd		*passwd;
	t_group			*gr;
	unsigned int	mj[2];

	if (!(passwd = getpwuid(buf->st_uid)))
		return ;
	if (!(gr = getgrgid(passwd->pw_gid)))
		return ;
	printf("Proprietaire: %s\n", passwd->pw_name);
	printf("Groupe : %s\n", gr->gr_name);
	printf("Taille : %lld\n", buf->st_size);
	printf("nb block : %lld\n", buf->st_blocks);
	mj[0] = (__int32_t)(((__uint32_t)buf->st_rdev >> 24) & 0xff);
	mj[1] = (__int32_t)((buf->st_rdev) & 0xffffff);
	printf("Id : %#x %u %u, %d %d\n", buf->st_rdev, mj[0], mj[1],(__uint32_t)buf->st_rdev >> 24, (buf->st_rdev) & 0xffffff);
}

void	ft_print_time(t_stat *buf)
{
	printf("Dernier changement d’état (modif general)         :  %s", ctime(&buf->st_ctime));
    printf("Dernier accès au fichier (creation)               :  %s", ctime(&buf->st_atime));
    printf("Dernière modification du fichier (modif contenue) :  %s", ctime(&buf->st_mtime));
}

void	ft_printf_stat(t_stat *buf)
{
	printf("type : ");
	ft_print_type(buf);
	printf("\nmode : ");
	ft_print_mode_i(buf);
	printf("nb lien :%hu\n", buf->st_nlink);
	ft_print_info(buf);
	ft_print_time(buf);
}

int		main(int ac, char **av)
{
	t_stat buf;

	// if (lstat(".", &buf))
	// 	return (1);
	// printf("%s\n", ".");
	// ft_printf_stat(&buf);
	// printf("\n");
	// if (lstat("..", &buf))
	// 	return (1);
	// printf("%s\n", "..");
	// ft_printf_stat(&buf);
	// printf("\n");
	if (ac <= 1)
	{
		printf("./ft_inspect_file <file>\n");
		return (0);
	}
	av++;
	while (*av)
	{
		if (stat(*av, &buf))
			return (1);
		printf("%s\n", *av);
		ft_printf_stat(&buf);
		printf("\n");
		av++;
	}
	return (0);
}
