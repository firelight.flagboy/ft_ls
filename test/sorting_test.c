/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sorting_test.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/15 12:57:41 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/15 13:30:55 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void		simple_sort(int *tab, int n, size_t *cycle)
{
	int		i;

	i = 0;
	while (i < n - 1)
	{
		if (tab[i] > tab[i + 1])
		{
			swap(tab + i, tab + i + 1);
			*cycle = *cycle + 1;
			i = 0;
		}
		else
			i++;
	}
}

void	buble_sort(int	*tab, int n, size_t *cycle)
{
	int	i, j;
	char	sp;

	i = 0;
	while (i < n - 1)
	{
		j = 0;
		sp = 0;
		while (j < n - i -1)
		{
			if (tab[j] > tab[j + 1])
			{
				swap(tab + j, tab + j + 1);
				sp = 1;
				*cycle = *cycle + 1;
			}
			j++;
		}
		if (sp == 0)
			break;
		i++;
	}
}

int		main(void)
{
	int		tab[23] = {22,15,35,97,102,456,547,135,354,651,357,6513,84713,8521054,1456,123,10,5,4,874,623,456,1305};
	int		i = 0;
	size_t	cycle = 0;

	while (i < 23)
		printf("%-*d", 8, tab[i++]);
	printf("\nend\n\n");
	i = 0;
	simple_sort(tab, 23, &cycle);
	printf("cycle :%zu\n\n", cycle);
	while (i < 23)
		printf("%-*d", 8, tab[i++]);
	printf("\n");
}
