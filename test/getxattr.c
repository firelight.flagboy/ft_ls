/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getxattr.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/17 09:05:49 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/17 10:18:45 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** code from "https://chromium.googlesource.com/chromiumos/platform/punybench/+/780.B/file.m/getxattr.c"
*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <errno.h>
// #include <attr/xattr.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>
#include <locale.h>
#include <string.h>
#include <unistd.h>

#ifndef TRUE
# define TRUE 1
#endif
#ifndef FALSE
# define FALSE 0
#endif
#ifndef XATTR_SIZE
# define XATTR_SIZE 10000
#endif
/* dumpmem: dumps an n byte area of memory to screen */
void dumpmem (const void *mem, unsigned int n)
{
	enum { NLONGS = 4,
		NCHARS = NLONGS * sizeof(long) };
	const unsigned long	*p = mem;
	const char		*c = mem;
	unsigned		i, j;
	unsigned		q, r;
	q = n / NCHARS;
	r = n % NCHARS;
	for (i = 0; i < q; i++) {
		printf("%8p:", p);
		for (j = 0; j < NLONGS; j++) {
			printf(" %8lx", *p++);
		}
		printf(" | ");
		for (j = 0; j < NCHARS; j++, c++) {
			printf("%c", isprint(*c) ? *c : '.');
		}
		printf("\n");
	}
	if (!r) return;
	printf("%8p:", p);
	for (j = 0; j < r / sizeof(long); j++) {
		printf(" %8lx", *p++);
	}
	for (; j < NLONGS; j++) {
		printf("         ");
	}
	printf(" | ");
	for (j = 0; j < r; j++, c++) {
		printf("%c", isprint(*c) ? *c : '.');
	}
	printf("\n");
}

int main (int argc, char *argv[])
{
	char	list[XATTR_SIZE], value[XATTR_SIZE];
	ssize_t	listlen, valuelen;
	char	hexdysplay = 0;
	int		j, k, ns;

	argc--;
	argv++;
	for (j = 0; j < argc; j++) {
		if ((listlen = listxattr(argv[j], list, XATTR_SIZE, XATTR_NOFOLLOW)) == -1) {
			printf("listxattr file :%s error:%s\n", argv[j], strerror(errno));
			continue ;
		}
		printf("file :%s\n", argv[j]);
		for (ns = 0; ns < listlen; ns += (strlen(list + ns) + 1)) {
			printf("%8s :%s ", "name", list + ns);
			valuelen = getxattr(argv[j], list + ns, value, XATTR_SIZE, 0, 0);
			printf("valuelen :%ld\n", valuelen);
			if (valuelen == -1) {
				printf("getxattr file :%s error:%s\n", argv[j], strerror(errno));
				continue ;
			} else if (!hexdysplay) {
				printf("value :%.*s\n", (int)valuelen, value);
			} else {
				printf("value :");
				for (k = 0; k < valuelen; k++) {
					printf("%02x ", (unsigned int)value[k]);
				}
				printf("\n");
			}
		}
	}
	return 0;
}
