/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_dir.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/27 13:37:11 by fbenneto          #+#    #+#             */
/*   Updated: 2018/01/08 10:17:10 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

typedef struct dirent t_dirent;

int		main(int ac, char **av)
{
	DIR *dd;
	t_dirent *dirent;

	if (ac <= 1)
	{
		printf("./ft_list_dir <dir>\n");
		return (0);
	}
	av++;
	while (*av)
	{
		if (!(dd = opendir(*av)))
		{
			printf("ft_ls: %s: %s\n", *av, strerror(errno));
			return (1);
		}
		printf("%s:\n", *av);
		while ((dirent = readdir(dd)))
			printf("%-50s %hhu\n",\
			dirent->d_name, dirent->d_type);
		av++;
		printf("%c", (*av) ? '\n' : '\0');
	}
	closedir(dd);
	return (0);
}
