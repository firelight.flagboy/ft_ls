# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/12/27 12:36:31 by fbenneto          #+#    #+#              #
#    Updated: 2018/01/22 14:56:17 by fbenneto         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

##########
# TARGET #
##########

NAME=ft_ls

#######
# LIB #
#######

LIB_PRINTF_DIR =\
	ft_printf/
LIB_PRINTF_NAME =\
	libftprintf.a

LIB_PRINTF = $(addprefix $(LIB_PRINTF_DIR), $(LIB_PRINTF_NAME))
############
# COMPILER #
############

CC =gcc
CFLAGS =-Wall -Werror -Wextra -O3

##########
# HEADER #
##########

INC_DIR_PRINTF =\
	./ft_printf/includes/
INC_NAME_PRINTF =\
	ft_printf_typedef.h\
	ft_printf_header_fc.h\
	ft_printf_d.h\
	ft_printf.h\

INCLUDE_PRINTF = $(addprefix $(INC_DIR_PRINTF), $(INC_NAME_PRINTF))
INC_PRINTF = -I $(INC_DIR_PRINTF)
INC_DIR =\
	./includes/
INC_NAME = \
	ft_ls.h\
	ft_ls_define.h\
	ft_ls_struct.h\
	ft_ls_typedef.h\
	ft_ls_o_include.h\

INCLUDE = $(addprefix $(INC_DIR), $(INC_NAME))
INC = -I $(INC_DIR)

#######
# SRC #
#######

SRC_DIR = ./srcs/
SRC_NAME=\
	ft_main.c\
	ft_error.c\
	ft_get_option.c\
	ft_itoa_st.c\
	ft_print_col.c\
	ft_get_mode.c\
	ft_str_2.c\
	ft_str.c\
	ft_ls_n_r.c\
	ft_print_res_g.c\
	ft_put_color.c\
	ft_ls_rec_2.c\
	ft_ls_file.c\
	ft_inter_opt.c\
	ft_init_struct.c\
	ft_singleton.c\
	ft_copy_data.c\
	ft_get_mode_2.c\
	ft_list_xattr.c\
	ft_list.c\
	ft_put.c\
	ft_warnx.c\
	ft_ls_d.c\
	ft_ls.c\
	ft_print_res.c\
	ft_ls_rec.c\
	ft_routine.c\
	ft_cmp.c\
	ft_sort_merge.c\
	ft_get_put.c\
	ft_sort_param.c\

SRC = $(addprefix $(SRC_DIR), $(SRC_NAME))

#######
# OBJ #
#######

OBJ_DIR= ./obj/
OBJ_NAME=$(SRC_NAME:.c=.o)
OBJ=$(addprefix $(OBJ_DIR), $(OBJ_NAME))

#########
# MACRO #
#########

NC		= "\\033[0m"
RED		= "\\033[31m"
GREEN	= "\\033[32m"
YELLOW	= "\\033[33m"
BLUE	= "\\033[34m"
MAJENTA	= "\\033[35m"
CYAN	= "\\033[36m"
BOLD	= "\\033[1m"
CHEK	= "\\xE2\\x9C\\x94"
OK		= "$(GREEN)$(CHEK)$(NC)"

#########
# RULES #
#########

all : $(NAME)

$(NAME) : $(OBJ_DIR) $(OBJ) $(LIB_PRINTF)
	@printf "[ft_ls]     compile binary $(BOLD)$(CYAN)%-23s" "$@"
	@$(CC) $(CFLAGS) -o $@ $(OBJ) $(LIB_PRINTF)
	@printf $(NC)$(OK)'\n'

$(OBJ_DIR) :
	@printf "[ft_ls]     creating folder $(MAJENTA)$(BOLD)%-22s" "obj"
	@mkdir -p $(OBJ_DIR)
	@printf $(NC)$(OK)'\n'

$(OBJ_DIR)%.o: $(SRC_DIR)%.c $(INCLUDE) $(INCLUDE_PRINTF)
	@printf "\033[0m[ft_ls]     compile $(BOLD)$(YELLOW)%-30s" "$<"
	@$(CC) $(CFLAGS) -o $@ -c $< $(INC) $(INC_PRINTF)
	@printf $(NC)$(OK)'\n'

$(LIB_PRINTF) :
	@make -C $(LIB_PRINTF_DIR) $(LIB_PRINTF_NAME)

clean : $(OBJ_DIR)
	@make -C $(LIB_PRINTF_DIR) clean

	@printf "[ft_ls]     rm all $(BOLD)$(RED)%-31s" "obj file"
	@rm -rf $(OBJ_DIR)
	@printf $(NC)$(OK)'\n'

norme : $(SRC) $(INCLUDE)
	@make -C $(LIB_PRINTF_DIR) norme
	@printf "checking\n"
	@norminette $^

fclean : clean
	@make -C $(LIB_PRINTF_DIR) naelc
	@printf "[ft_ls]     rm $(BOLD)$(CYAN)%-35s" "$(NAME)"
	@rm -f $(NAME)
	@printf $(NC)$(OK)'\n'

proper :
	@make -C ./ all
	@make -C ./ clean

re :
	@make -C ./ fclean
	@make -C ./ all

.PHONY: proper re norme all fclean clean naelc
